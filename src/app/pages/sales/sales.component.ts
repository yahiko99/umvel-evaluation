import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service'
import { Sales } from '../../models/Sales.model';
import { ChartDataSets, ChartOptions, ChartPluginsOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { NumbersData } from 'src/app/models/NumbersData.model';


@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.sass']
})
export class SalesComponent implements OnInit {

  
  // totalsale: number;
  numdata: Array<number> = new Array<number>();
  namedata: Array<string> = new Array<string>();
  sales: Array<Sales> = new Array<Sales>();
 
 
  lineChartData: ChartDataSets[] = [
      {
        data:          this.numdata,
        
        label: "Salarios"
      }
    ];

    lineChartLabels: Label[] = this.namedata
    
    lineChartOptions: ChartOptions  = {
      responsive : true
    }
    
    lineChartType: ChartType = 'line'

    lineChartPlugins = [];



    constructor(
    private clientService: ClientService
    ) { 
   
    }

    ngOnInit(): void {
      this.clientService.getSalesData().subscribe((dataSales: NumbersData) => {
          this.sales = dataSales.sales
          
          console.log('Recibiendo data desde Api', dataSales.sales);

          dataSales.sales.forEach((element: any) => {  
             
            
            this.numdata.push(element.quantity)
            this.namedata.push(element.car_make)
            
                   
          });
      })
   }



    
  }
  


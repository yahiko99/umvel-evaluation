import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ClientService } from '../../services/client.service'
import { Client } from '../../models/Client.model';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.sass']
})
export class PrincipalComponent implements OnInit {

clients: Array<Client> = new Array<Client>();

showFiller = false;
  
  constructor(
    private clientService: ClientService, 
  ) { }

  ngOnInit(): void {

    this.clientService.getClient().subscribe((dataClient: any) => {
        this.clients = dataClient.users
       console.log('Recibiendo data desde Api', dataClient.users.length);
    })
  }

  dataClient: Client[] = []

  handlePage(e: PageEvent){
    this.page_size = e.pageSize
    this.page_number = e.pageIndex + 1
  }

  page_size: number = 5 
  page_number: number = 1
  pageSizeOptions = [5, 10, 20, 50, 100]

}





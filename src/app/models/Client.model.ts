import { User } from './User.model';
export interface Client{

id: number;
first_name: string;
last_name: string;
email: string;
gender: string;
users: Array<User>;
image: string;
 
}

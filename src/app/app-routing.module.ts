import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './pages/register/register.component';
import { AuthenticateComponent } from './pages/authenticate/authenticate.component';
import { PrincipalComponent } from './pages/principal/principal.component'
import { SalesComponent } from './pages/sales/sales.component'
const routes: Routes = [

{ path: 'register', component: RegisterComponent },
{ path: 'authenticate', component: AuthenticateComponent },
{ path: 'principal', component: PrincipalComponent },
{ path: 'sales', component: SalesComponent },
// { path: '', pathMatch: 'full', redirectTo: 'authenticate' },
// { path: '**', pathMatch: 'full', redirectTo: 'authenticate' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }

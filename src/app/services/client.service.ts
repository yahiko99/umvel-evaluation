import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { NumbersData } from '../models/NumbersData.model';
import { Client } from './../models/Client.model';
import { Sales } from './../models/Sales.model';

@Injectable({
    providedIn: 'root'
  })
  export class ClientService {

    private _search$: Subject<string> = new Subject<string>();   
  
    constructor( private http: HttpClient ) { }    
  
    get search$(): Observable<string> {
      return this._search$;
    }
  
    changeSearch(search: string): void {
      this._search$.next(search);
    }
  
    getClient(): Observable <Client> {
    
      console.log('Servicio Clientes:', `https://run.mocky.io/v3/d5ddf1ff-a0e2-4a7e-bbcc-e832bef6a503`);
      return this.http.get<Client>(`https://run.mocky.io/v3/d5ddf1ff-a0e2-4a7e-bbcc-e832bef6a503`);
    }
  
    getSalesData(): Observable <NumbersData>{
        console.log('Servicio Ventas:', `https://run.mocky.io/v3/15517ca5-7e07-4ebc-bf63-5b033ec4e16a`);
        return this.http.get<NumbersData>(`https://run.mocky.io/v3/15517ca5-7e07-4ebc-bf63-5b033ec4e16a`);
    }     
  }
  